node /^juamq\d{2}p1\.mum1\.rjil\.com$/ {
# Pending activemq version for CentOS upgrade
    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "cms-nodes",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Jukebox Arlanda ActiveMQ",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "mobi_cms_us::juamq":
        ensure  => "running",
        version => "5.9.0-272600",
        enable  => true,
        is_59   => true,
        activemq_username => "activemq",
        activemq_password => "U9jsF267b_djgre",
        #activemq_db_jdbc_url => "jdbc:mysql://mydtbtransientvip:3307/activemq?relaxAutoCommit=true",
        require =>  Class["java"],
    }

}
