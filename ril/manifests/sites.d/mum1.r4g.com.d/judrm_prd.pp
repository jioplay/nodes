node  /^judrm\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "drm-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp                => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Jukebox DRM",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "java":
        jdk_pkg     => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver     => "present",
    }

    class { "tomcat":
        package                 => "mobi-tomcat-config-8080",
        version                 => "6.0.35-211717",
        override_catalina       => false,
        server_config_loc       => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src       => "server.xml.tomcat8080",
        config_owner            => "rtv",
        config_group            => "rtv",
        jars                    => [ "c3p0-0.9.2-pre2.jar", "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        initscript              => "tomcat8080",
        require                 => Class["java"],
        jrehome                 => "",
    }

    class { "mobi_roapserver":
        version      => "2.1.0-1273",
        jdbc_driver  => "com.mysql.jdbc.Driver",
        validation_query => "select 1",
        lm_user      => "lm_user",
        lm_user_pass => "jkoi_erFGu6dw9",
        db_url       => "jdbc:mysql://mydtbvip:3306/licensemanager?autoReconnect=true",
        require      => Class["tomcat"],
    }

    class { "mobi_ocspresponder" :
        drm2_1       => true,
        version      =>"2.1.0-1128",
        licensemanager_keystore_dir => "/opt/drm/conf/keystore",
        jdbc_database_type => "MYSQL",
        jdbc_driver  => "com.mysql.jdbc.Driver",
        lm_user      => "ocsp_user",
        lm_user_pass => "nf_H234_58jkl",
        db_url       => "jdbc:mysql://mydtbvip:3306/ocspresponder?autoReconnect=true",
        validation_query => "select 1",
        max_idle     => "20",
        use_revocation2 => true,
    }

    class { "memcached":
        version => "1.4.4"
    }

    package { "mobi-server-drm-keystore-ril":
        ensure  => "1.0.1-1298",
        require => [Class["mobi_licensemanager2"],Class["mobi_roapserver"],Class["mobi_ocspresponder"]],
    }

    class { "mobi_licensemanager2" :
        version             => "2.1.0-1273",
        db_type             => "MYSQL",
        authnz_enabled      => false,
        authnz_identity_url => "http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
        authnz_rights_url   => "http://rightsmanagervip:8080/mobi-aaa-rights-manager",
        ri_url              => "http://data.jioplay.com/services/roap",
        rs_rights_url       => "http://rightsmanagervip:8080/mobi-aaa-rights-manager/core/v5/rights",
        rs_ocsp_url         => "http://localhost:8080/ocspresponder/services/ocsp/request",
        lm_user             => "lm_user",
        lm_user_pass        => "jkoi_erFGu6dw9",
        jdbc_driver         => "com.mysql.jdbc.Driver",
        validation_query    => "select 1 from dual",
        db_url              => "jdbc:mysql://mydtbvip:3306/licensemanager?autoReconnect=true",
        memcached_host_list =>         "judrm01p1:11211,judrm02p1:11211,judrm03p1:11211,judrm04p1:11211,judrm05p1:11211,judrm06p1:11211,judrm07p1:11211,judrm08p1:11211,judrm09p1:11211,judrm10p1:11211,judrm11p1:11211,judrm12p1:11211,judrm13p1:11211,judrm14p1:11211,judrm15p1:11211,judrm16p1:11211,judrm17p1:11211,judrm18p1:11211,judrm19p1:11211,judrm20p1:11211,judrm21p1:11211,judrm22p1:11211,judrm23p1:11211,judrm24p1:11211,judrm25p1:11211,judrm26p1:11211,judrm27p1:11211,judrm28p1:11211,judrm29p1:11211,judrm30p1:11211,judrm31p1:11211,judrm32p1:11211,judrm33p1:11211,judrm34p1:11211,judrm35p1:11211,judrm36p1:11211,judrm37p1:11211,judrm38p1:11211,judrm39p1:11211,judrm40p1:11211,judrm41p1:11211,judrm42p1:11211,judrm43p1:11211,judrm44p1:11211,judrm45p1:11211,judrm46p1:11211,judrm47p1:11211,judrm48p1:11211,judrm49p1:11211,judrm50p1:11211",
        require             => Class["tomcat"],
    }

}





