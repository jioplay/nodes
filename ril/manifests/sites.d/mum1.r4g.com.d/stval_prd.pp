node "ril-prod-stval" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Live-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }
    class { "apache":
        icinga          => false,
        service_ensure  => "stopped",
        service_enable  => false,
    }
    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "stval",
        hosttype    => "vm",
        stage       => runtime,
    }

     varnish::healthcheck { "fmp4_health":
        url => "/check.txt",
        interval => "5s",
        timeout => "1s",
        window => 4,
        threshold => 3,
        initial => 3,
        response => 200,
    }

    varnish::healthcheck { "hls_health":
        url => "/check.txt",
        interval => "5s",
        timeout => "1s",
        window => 4,
        threshold => 3,
        initial => 3,
        response => 200,
    }

    varnish::pool { "fmp4_live_01":
        backends => ["stfml08p1.${::domain}","stfml09p1.${::domain}"],
        port => "80",
        healthcheck => "fmp4_health",
        method => "round-robin",
    }
    varnish::pool { "fmp4_live_02":
        backends => ["stfml01p2.${::domain}","stfml02p2.${::domain}"],        
        port => "80",
        healthcheck => "fmp4_health",
        method => "round-robin",
    }

     varnish::pool { "fmp4_live_03":
        backends => ["stfml01p3.${::domain}","stfml02p3.${::domain}"],      
        port => "80",
        healthcheck => "fmp4_health",
        method => "round-robin",
    }
      varnish::pool { "fmp4_live_04":
        backends => ["stfml01p4.${::domain}","stfml02p4.${::domain}"],       
        port => "80",
        healthcheck => "fmp4_health",
        method => "round-robin",
    }

     varnish::pool { "hls_live_01":
        backends => ["stmxl01p1.${::domain}","stmxl03p1.${::domain}","stmxl04p1.${::domain}"],
        port => "80",
        healthcheck => "hls_health",
        method => "round-robin",
    }



    varnish::channel_rule { "fmp4_rule_01":
        channels => ["1", "2", "3", "4", "5", "6", "8", "9", "10", "11", "12",  "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "28", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "61", "62", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "78", "79", "80","81", "82", "83", "85", "86", "87", "88", "89", "90", "502"],
        regex => "^/(live|record_master)/[^/]*/",
        pool => "fmp4_live_01",
    }

     varnish::channel_rule { "fmp4_variant_rule_01":
        channels => [],
        regex => "^/(variants)/(22|78|73|69|95|80|81|82|86|36|107|35|83|85|65|323|351|383|235|261|477|478|481|484|488|489|502|503)$",
        response => "error 750 ",
    }


     varnish::channel_rule { "fmp4_rule_02":
        channels => ["91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104","107", "108", "109", "112", "113", "114","200", "201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220", "221", "222", "223", "224", "225", "226", "227", "228", "229", "230", "231", "232", "233", "234", "235", "236", "237", "238", "239", "240", "241", "242", "243", "244", "245", "246", "247","250", "251", "252", "253", "254", "255", "256", "257", "258", "259", "260", "261", "262", "263", "503", "504"],
        regex => "^/(live|record_master)/[^/]*/",
        pool => "fmp4_live_02",
    }

        varnish::channel_rule { "fmp4_variant_rule_02":
        channels => [],
        regex => "^/(variants)/(1|2|3|4|5|6|8|9|10|12|15|16|17|18|19|20|21|23|24|25|26|28|30|31|32|33|34|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|61|62|64|66|67|68|70|71|72|74|75|76|79|87|88|89|90|91|92|93|94|96|97|98|99|100|101|102|103|104|108|109|112|113|114|200|201|202|203|204|205|206|207|208|209|210|211|212|213|214|215|216|217|218|219|220|221|222|223|224|225|226|227|228|229|230|231|232|233|234|236|237|238|239|240|241|242|243|244|245|246|247|250|251|252|253|254|255|256|257|258|259|260|262|263|264|300|301|302|303|304|305|306|307|308|309|310|311|312|313|314|315|316|317|318|319|320|321|322|324|325|326|327|328|329|330|331|332|333|334|335|336|337|338|339|340|341|343|344|345|346|347|348|349|350|353|354|355|361|362|363|364|365|366|369|370|371|372|373|374|375|376|377|378|379|380|381|382|383|384|385|386|387|388|389|390|391|392|393|394|395|396|397|398|399|400|401|402|403|404|405|406|407|408|409|410|411|412|413|414|415|416|417|418|419|420|421|422|423|424|425|426|427|428|429|430|431|432|433|434|435|439|440|441|442|443|444|445|447|448|449|450|451|452|453|454|455|456|457|459|460|461|462|463|464|465|466|467|468|471|472|473|474|475|480|482|483|485|486|487|490|491|492|493|494|495|496|497|498|499|504)$",
        response => "error 751 ",
    }

    varnish::channel_rule { "fmp4_rule_03":
        channels => ["264","300", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "311", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "322", "323", "324", "325", "326", "327", "328", "329", "330", "331", "332", "333", "334", "335", "336", "337", "338", "339", "340", "341", "343", "344", "345", "346", "347", "348", "349", "350", "351", "353", "354", "355","361", "362", "363", "364", "365", "366", "369", "370", "371", "372", "373", "374", "375", "376", "377", "378", "379", "380", "381", "382", "383", "384", "385", "386", "387", "388", "389", "390", "391", "392", "393", "394", "395", "396", "397", "398", "399"],
        regex => "^/(live|record_master)/[^/]*/",
        pool => "fmp4_live_03",
    }

   varnish::channel_rule { "fmp4_rule_04":
        channels   => ["400", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "411", "412", "413", "414", "415", "416", "417", "418", "419", "420", "421", "422", "423", "424", "425", "426", "427", "428", "429", "430", "431", "432", "433", "434", "435", "439", "440", "441", "442", "443", "444", "445", "447", "448", "449", "450", "451", "452", "453", "454", "455", "456", "457", "459", "460", "461", "462", "463", "464", "465", "466", "467", "468", "471", "472", "473", "474", "475", "477", "478", "480", "481", "482", "483", "484", "485", "486", "487", "488","489", "490", "491", "491", "492", "493", "494", "495", "496", "497", "498", "499"],
         regex => "^/(live|record_master)/[^/]*/",
        pool => "fmp4_live_04",

    }
   
    varnish::channel_rule { "fmp4_monitoring":
        channels => [],
        regex    => "^/(live|record_master)/monitoring/health$",
        pool     => "fmp4_live_01",
    }

      varnish::channel_rule { "hls_live_rule_01":
        channels => [],
        regex => "^/live_hls.*",
        pool => "hls_live_01",
    }

     varnish::variant_cache { "variant_cache_01":
         regex => "variants",
         status => "751",
         ttl => "14400m",
    }

    class { "varnish" :
        package => "varnish-3.0.4-1.el6",
        package_ensure => "present",
        configs => ["ril/haproxy-fmp4-cache.vcl","ril/haproxy-live-fmp4-cache.vcl","ril/drm.ril-prod.vcl"],
        varnish_sysconfig => "ril/varnish.haproxy-load.sysconfig",
        varnish_storage_size => "2G",
        varnish_listen_port => "8080",
        default_backend_port => "80",
        require => [
                   Varnish::Channel_rule["hls_live_rule_01"],
                  Varnish::Channel_rule["fmp4_rule_01"],
                  Varnish::Channel_rule["fmp4_variant_rule_01"],
                 Varnish::Channel_rule["fmp4_rule_02"],
                Varnish::Channel_rule["fmp4_variant_rule_02"],
                    Varnish::Channel_rule["fmp4_rule_03"],
                    Varnish::Channel_rule["fmp4_rule_04"],
                    Varnish::Channel_rule["fmp4_monitoring"],
                    Varnish::Healthcheck["fmp4_health"],
                    Varnish::Healthcheck["hls_health"],
                    Varnish::Pool["fmp4_live_01"],
                   Varnish::Pool["fmp4_live_02"],
                   Varnish::Pool["fmp4_live_03"],
                   Varnish::Pool["fmp4_live_04"],
                   Varnish::Pool["hls_live_01"],
                   Varnish::Variant_cache["variant_cache_01"],
                     ],
  }
  file { "/etc/varnish/HD.json":
      owner   => root,
      group   => root,
      mode    => "0644",
      source => "puppet://$puppetserver/modules/varnish/HD.json",
  }

  file { "/etc/varnish/SD.json":
      owner   => root,
      group   => root,
      mode    => "0644",
      source => "puppet://$puppetserver/modules/varnish/SD.json",
  }
}

node "stval01p1.mum1.r4g.com" inherits "ril-prod-stval" {

     class { "haproxy":
        config_template => "haproxy_varnish_load.cfg.erb",
        bind_port => "80",
        backend_port => "8080",
        balance_list => ["stval02p1.${::domain}", "stval03p1.${::domain}", "stval04p1.${::domain}", "stval05p1.${::domain}"],
        manage_package => "1.4.22-5.el6_4",
    }
}

node "stval02p1.mum1.r4g.com" inherits "ril-prod-stval" {

     class { "haproxy":
        config_template => "haproxy_varnish_load.cfg.erb",
        bind_port => "80",
        backend_port => "8080",
        balance_list => ["stval03p1.${::domain}", "stval04p1.${::domain}", "stval05p1.${::domain}"],
        manage_package => "1.4.22-5.el6_4",
    }
}

node "stval03p1.mum1.r4g.com" inherits "ril-prod-stval" {

     class { "haproxy":
        config_template => "haproxy_varnish_load.cfg.erb",
        bind_port => "80",
        backend_port => "8080",
        balance_list => ["stval02p1.${::domain}", "stval04p1.${::domain}", "stval05p1.${::domain}"],
        manage_package => "1.4.22-5.el6_4",
    }
}

node "stval04p1.mum1.r4g.com" inherits "ril-prod-stval" {

    class { "haproxy":
        config_template => "haproxy_varnish_load.cfg.erb",
        bind_port => "80",
        backend_port => "8080",
        balance_list => ["stval02p1.${::domain}", "stval03p1.${::domain}", "stval05p1.${::domain}"],
        manage_package => "1.4.22-5.el6_4",
    }
}

node "stval05p1.mum1.r4g.com" inherits "ril-prod-stval" {

     class { "haproxy":
        config_template => "haproxy_varnish_load.cfg.erb",
        bind_port => "80",
        backend_port => "8080",
        balance_list => ["stval02p1.${::domain}", "stval03p1.${::domain}", "stval04p1.${::domain}"],
        manage_package => "1.4.22-5.el6_4",
    }
}
