node /^inmon\d{2}\.staging\.mum1\.r4g\.com$/ {

    class { "base" :
        host_type               => "ril_qa",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Admin-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "small test machine",
        hosttype    => "vm",
        stage       => runtime,
    }
   class { "apache":
     icinga          => true,
     icinga_instance => "icingap1.ppd-ril.qa.smf1.mobitv",
     icinga_cmd_args => " -H $::fqdn -u /icinga/ -a icingaadmin:e0rtv12 -w 2 -c 3",
   }

   # Declare icinga server with params for a "central" server.
   class { "icinga":
     apache_conf              => "icinga.conf.ril.staging",
     execute_service_checks   => "1", # disable
     execute_host_checks      => "1", # disable
     process_performance_data => "0", # disable
     enable_notifications     => "1", # enable
     central                  => true, # give me all generated configs
   }

   # generate static icinga host and service checks.
#   class { "icinga::static::ril": }

   class { "icinga::nsca": }

}
