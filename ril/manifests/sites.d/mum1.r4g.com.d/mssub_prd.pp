node /^mssub\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Auth-Subscriptions-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Subscriber",
        hosttype    => "vm",
        stage       => runtime,
    }

     class { "java":
            jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
            jdk_ver => "present",
    }


    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-209592",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        jars               => [ "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
    }

     class { "mobi_aaa_purchase_manager":
        mobi_aaa_purchase_manager_package_name => "mobi-aaa-ril-purchase-manager",
        version => "5.5.0-201502030555.2beb322",
        database_url => "jdbc:mysql://mydtbvip:3306/purchase_manager",
        hibernate_dialect => "org.hibernate.dialect.MySQL5Dialect",
        database_driver => "com.mysql.jdbc.Driver",
        database_user_name => "purchase_user",
        database_password => "lk_40jk904Mkf_dD",
        auth_manager_endpoint_url => "http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
        identity_manager_endpoint_url => "http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
        offer_manager_endpoint_url => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager",
        rights_manager_endpoint_url => "http://rightsmanagervip:8080/mobi-aaa-rights-manager",
        transaction_processor_endpoint_url => "http://localhost:8080/mobi-aaa-transaction-processor",
        jersey_restclient_max_connections => "3500",
        database_datasource_initialsize => "150",
        database_datasource_maxactive => "300",
        database_datasource_maxidle => "200",
        carrier_vendor => ["infotel=reliance",],
        require => Class["tomcat"],
    }
    class { "mobi_aaa_ril_purchase_manager_adapter":
        mobi_aaa_ril_purchase_manager_adapter_version => "5.3.0-274973",
        #mobi_aaa_ril_purchase_manager_adapter_version => "5.3.0-272397",
        rpsl_server_url => "https://test.rpay.co.in/api/merchants/",
        purchase_url => "/spend",
        http_proxy_enabled => false,
        http_proxy_host_address => "10.48.2.36",
        retry_rules_enabled => true,
        check_balance_url => "/userbalance",
        notify => Class["tomcat::service"],
        require => [ Class["tomcat"], Class["mobi_aaa_purchase_manager"],],
    }

    class { "aaa_txprocessor":
        txprocessor_ver => "5.4.0-264935",
        offer_manager_endpoint_url => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager",
        db_url          => "jdbc:mysql://mydtbvip:3306/purchase_manager",
        db_username     => "purchase_user",
        db_password     => "lk_40jk904Mkf_dD",
        db_driver       => "com.mysql.jdbc.Driver",
        hibernate_dialect  => "org.hibernate.dialect.MySQLDialect",
        hibernate_show_sql => "false",
        pending_enable => "true",
        mrc_enable => "true",
        mrc_cron => "0 0/4 * * * ?",
        jersey_restclient_connection_timeout => "120",
        jersey_restclient_read_timeout => "120",
        cm_transaction_processor_jerseyClient_connectionTimeoutSeconds => "120",
        cm_transaction_processor_jerseyClient_readTimeoutSeconds => "120",
        config_file => "/opt/mobi-aaa-transaction-processor/conf/application-config-override.xml",
        adapter_mappings => ["infotel-reliance=http://purchasemanageradaptervip:8080/mobi-aaa-ril-purchase-manager-adapter",],
        require => [Class["tomcat"]],
    }

    class { "mobi_aaa_ril_selfcare_adapter":
        version => "5.3.0-201407061240.39d9abd",
        #version => "5.3.0-272739",
        database_url => "jdbc:mysql://mydtbvip:3306/rights_manager",
        database_user_name => "rights_user",
        database_password => "kl_rkl68P_GJfdf",
        database_driver => "com.mysql.jdbc.Driver",
        hibernate_dialect => "org.hibernate.dialect.MySQLDialect",
        hibernate_show_sql => "false",
        require => [ Class["tomcat"] ],
    }

}
