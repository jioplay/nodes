node "ril-prod-juslr" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "database-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Jukebox Solar",
        hosttype    => "vm",
        stage       => runtime,
    }

     class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-211717",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
        initheapsize       => "512",
        heapsize           => "2048",
        permgensize        => "128",
    }

    file { "schema.xml":
        path    => "/opt/mobitv/solr/conf/schema.xml",
        source  => "puppet:///modules/mobi_guide_manager/solr/schema.xml",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
    }

    file { "wordDelimiterTypes.txt":
        path    => "/opt/mobitv/solr/conf/wordDelimiterTypes.txt",
        source  => "puppet:///modules/mobi_guide_manager/solr/wordDelimiterTypes.txt",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
    }
}

node "juslr01p1.mum1.rjil.com" inherits "ril-prod-juslr" {
    class { "solr":
        mode        => "master",
        version     => "5.0.0-194012",
        common_ver  => "5.3.0-256460",
        notify      => Class["tomcat::service"],
        require     => Class["tomcat"],
    }
}

node "juslr02p1.mum1.rjil.com" inherits "ril-prod-juslr" {
    class { "solr":
        mode        => "repeater",
        version     => "5.0.0-194012",
        common_ver  => "5.3.0-256460",
        notify      => Class["tomcat::service"],
        require     => Class["tomcat"],
    }
}

node "juslr03p1.mum1.rjil.com" inherits "ril-prod-juslr" {
     class { "solr":
          mode        => "master",
          version     => "5.0.0-194012",
          common_ver  => "5.3.0-256460",
          notify      => Class["tomcat::service"],
          require     => Class["tomcat"],
     }
}

node "juslr04p1.mum1.rjil.com" inherits "ril-prod-juslr" {
    class { "os::hosts":
        stage => setup,
        remote_hosts => [ "10.139.22.67 solrmastervip" ],
    }

    class { "solr":
            mode        => "repeater",
            version     => "5.0.0-194012",
            common_ver  => "5.3.0-256460",
            notify      => Class["tomcat::service"],
            require     => Class["tomcat"],
    }
}


