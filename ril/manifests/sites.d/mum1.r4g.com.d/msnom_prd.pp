node /^msnom\d{2}p1\.mum1\.rjil\.com$/ {
#Reconciliation Version (Final)
    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "others-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "msnom",
        hosttype    => "vm",
        stage       => runtime,
    }

     class {
        "apache" :
                package => "httpd.x86_64",
    }

    class { "java" :
            jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
            jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-211717",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
        heapsize           => "2048",
        permgensize        => "128",
    }

    class { "mobi_notification_message_adapter":
      mobi_message_adapter_solr_package => "mobi-message-adapter-solr-5.3.0-262205",
      mobi_message_adapter_subscriptions_package => "mobi-message-adapter-subscriptions-5.3.0-261864",
      require => Class["tomcat"],
    }

    class { "mobi_notification_push_adapter":
       mobi_push_adapter_gcm_package => "mobi-push-adapter-gcm-5.3.0-261680",
       mobi_push_adapter_apn_package => "mobi-push-adapter-apn-5.3.0-261863",
       mobi_push_adapter_mock_package => "mobi-push-adapter-mock-5.3.0-261679",
       require => Class["tomcat"],
    }
     class { "mobi_notification_manager":
        package => "mobi-notification-manager-5.3.0-261861",
        require => Class["tomcat"],
    }

    package{ "mobi-user-rtv-1.2.3-79211":
        ensure => present,
    }

    file { "mobi-push-adapter-apn/applicationContext.xml":
        path    => "/opt/mobi-push-adapter-apn/webapp/WEB-INF/classes/applicationContext.xml",
        source  => "puppet:///modules/mobi_notification_push_adapter/mobi-push-adapter-apn/applicationContext.xml",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0555",
        require => Class["mobi_notification_push_adapter"],
    }

    file { "mobi-push-adapter-apn/JioPlayEnterpriseAPNCertificate.p12":
        path    => "/opt/mobi-push-adapter-apn/webapp/WEB-INF/classes/JioPlayEnterpriseAPNCertificate.p12",
        source  => "puppet:///modules/mobi_notification_push_adapter/mobi-push-adapter-apn/JioPlayEnterpriseAPNCertificate.p12",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0755",
        require => Class["mobi_notification_push_adapter"],
    }

    file { "mobi-push-adapter-gcm/applicationContext.xml":
        path    => "/opt/mobi-push-adapter-gcm/spring.d/applicationContext.xml",
        source  => "puppet:///modules/mobi_notification_push_adapter/mobi-push-adapter-gcm/applicationContext.xml",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0555",
        require => Class["mobi_notification_push_adapter"],
    }
}
