node /^msprf\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "recording-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Profile",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache": }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.36-209007",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
    }

    class { "mobi_personalization_manager":
        package_ensure       => "5.5.0-201502021046.5062b5e",
        carrier              => "infotel",
        resource_server_type => "adapter",
        auth_manager_url     => "http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
        rights_manager_url   => "http://rightsmanagervip:8080/mobi-aaa-rights-manager",
        notify               => Class["tomcat::service"],
        require              => Class["tomcat"],
    }

    class { "mobi_recording_manager":
        mobi_recording_manager_version      => "5.3.0-201502030510.fb14358",
        vid_carrier                         => "infotel",
        vid_product                         => "r4g",
        vid_version                         => "5.0",
        vid_guideprovider                   => "default",
        is_auth_enabled                     => false,
        max_retention_period                => "365",
        amq_broker_recordings_url => "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100&startupMaxReconnectAttempts=10",
        amq_broker_netpvr_url => "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100&startupMaxReconnectAttempts=10",
        max_catchup_period                  => "8",
        recording_auth_service_name         => "core_v5_recmgr",
        recording_carrier_auth_service_name => "ril_program-manager3",
        is_shared_recording_enabled         => true,
        solr_master_url                     => "http://solrslavevip:8080/mobi-solr/",
        solr_master_timeout                 => "10000",
        storage_manager_url                 => "http://storagemanagervip:8080/mobi-storage-manager/",
        #notify                              => [ Class["tomcat::service"], Class["apache::service"] ],
        require                             => [ Class["tomcat"], Class["apache"] ],
    }

}
