node /^msfrn\d{2}p2\.mum1\.rjil\.com$/ {
#HTTPS Endpoint

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "others-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Frontend",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache" :
        package => "httpd.x86_64",
        listen => "2080",
    }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.36-209007",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
    }

    packages::realize_package{["git"]:}


    class { "ril_webapp" :
        version         => "2.0.38-2047863",
        package         => "reliance-trial-html",
        rewriteRule     => "^/jio/(.*)\$",
        proxy_pass_list => [
           "ProxyPassMatch /ril-api-html/identity/(.*)$ http://127.0.0.1/identity/\$1",
           "ProxyPassMatch /ril-api-html/guide/(.*)$ http://127.0.0.1/guide/\$1",
           "ProxyPassMatch /ril-api-html/core/v5/drm2/(.*)$ http://127.0.0.1/core/v5/drm2/\$1",
           "ProxyPassMatch /ril-api-html/services/roap/(.*)$ http://127.0.0.1/services/roap/\$1",
           "ProxyPassMatch /ril-api-html/core/v5/prefs/(.*)$ http://127.0.0.1/core/v5/prefs/\$1",
           "ProxyPassMatch /ril-api-html/core/(.*)$ http://127.0.0.1/core/\$1",
           "ProxyPassMatch /ril-api-html/manifest/(.*)$ http://fragservervodvip/manifest/\$1",
           "ProxyPassMatch /ril-api-html/rpsl/(.*)$ http://127.0.0.1/rpsl/\$1",
           "ProxyPassMatch /ril-api-html/livetovod/(.*)$ http://127.0.0.1/livetovod/\$1",
        ],
        require         => Class["apache"],
    }

   class { "app_upgrade_html" :
        version => "0.0.0.6-273643",
        package => "app-upgrade-html",
        rewrite_rule => "^/app-upgrade/(.*)$",
        proxy_pass_list => [
            "ProxyPassMatch /ril-api-upgrade-html/(.*)$ http://127.0.0.1/\$1",
        ],
         require         => Class["apache"],
     }

    class { "mobi_client_log_manager":
        mobi_client_log_manager_version => "5.0.0-209610",
        require => Class["tomcat"],
    }

    $mobi_rest_rpms = [ "mobi-restapi-core-v5-clientlog-5.0.1-213231",
                        "mobi-restapi-core-v5-config-5.0.1-196543",
                        "mobi-ril-restapi-v5-guide-5.1.0-201411041050.d1aeccd",
                        "mobi-mod-log-extras-4.7.0-168675",
                        #"mobi-mod-deflate-conf-5.3.0-245886.i686",
                        "mobi-restapi-v5-marketing-5.3.1-261703",
                        "mobi-restapi-core-v5-purchase-manager-adapter-5.0.3-263364",
			"mobi-restapi-core-v5-offer-manager-5.3.90-201407080841.e1861e1",
                        "mobi-restapi-v5-catalogue-5.3.0-248140",
			"mobi-restapi-core-v5-selfcare-adapter-5.3.90-201406060755.2dae030",
                        "mobi-restapi-core-v5-livetovod-adapter-5.3.90-273602",
                        "mobi-restapi-apache-global-4.7.80-153831",
                        "mobi-varnish-config-msfrn-3.0.3-221775",
                        "mobi-hls-playlists-vod-ril-v2-0.4.3-1415",
			"reliance-live2vod-html-2.0.33-000014",
			"mobi-restapi-core-v5-atomhopper-5.4.3-201409290528.cba177e",
                        "mobi-ril-recording-manager-varnish-rules-5.5.0-201502091323.96bc6c2",
                        "mobi-ril-purchase-manager-varnish-rules-5.5.0-201502040832.bdb8c8d",
                        "mobi-ril-personalization-manager-varnish-rules-5.5.0-201502061043.d6d5e15",
                        "mobi-ril-stream-manager-varnish-rules-5.5.0-201506011131.6e1e0ce",
                        "mobi-ril-drm-varnish-rules-5.5.0-201505061227.3f8c458",
                        "mobi-ril-recommendation-adapter-varnish-rules-5.5.0-201502031228.98ead36",
                        "mobi-ril-aaa-identity-manager-oauth2-varnish-rules-5.5.0-201502091254.ae52649",
                        "mobi-restapi-core-v5-resource-5.3.0-201505050416.e295a99",
                        ]

    os::motd::register { $mobi_rest_rpms: }


    package { $mobi_rest_rpms:
        ensure => "present",
        require => Class["apache"],
        notify => Class["apache::service"],
    }
    
    
#    vcsrepo { "/var/www/mobitv/resource/":
#        ensure => latest,
#        provider => git,
#        source => 'git://ingit01p1.mum1.rjil.com/client-resources.git',
#        revision => 'ril-prod',
#        require => [Package["git"]],
#    }
 
   class { "varnish" :
        package => "varnish-3.0.3-1.el6",
        package_ensure => "present",
        configs => ["msfrn/mobi-purchase-manager-bypass.vcl","msfrn/mobi-recommendation-adapter-bypass.vcl","msfrn/mobi-recording-manager-bypass.vcl","msfrn/mobi-ril-aaa-identity-manager-oauth2-bypass.vcl","msfrn/mobi-ril-drm-bypass.vcl","msfrn/mobi-stream-manager-bypass.vcl","msfrn/mobi-personalization-manager-bypass.vcl","msfrn/msfrn.vcl"],
        default_backend_port => "2080",
        varnish_listen_port => "80",
        varnish_storage_size => "2G",
        thread_pools => "8",
        session_linger => "50",
    }

    file { "mobi-guide-manager-proxy.conf":
        path    => "/etc/httpd/conf.d/mobi-guide-manager-proxy.conf",
        source  => "puppet:///modules/mobi_guide_manager/mobi-guide-manager-proxy.conf",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0755",
        require => Class["apache"],
    }
}
