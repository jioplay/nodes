node "inmon01p1.mum1.r4g.com" {
  class { "base":
    host_type               => "ril",
    manage_icinga           => true,
    manage_ganglia          => true,
    gmond_cluster_name      => "Admin-cluster",
    noop                    => false,
    puppetagent_environment => "qa_ril_5_4_0",
  }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "ICinga",
        hosttype    => "vm",
        stage       => runtime,
    }

  class { "apache":
    icinga          => true,
    icinga_instance => "inmon01p1.mum1.r4g.com",
    icinga_cmd_args => " -H $::fqdn -u /icinga/ -a icingaadmin:e0rtv12 -w 2 -c 3",
  }

  # Declare icinga server with params for a "central" server.
  class { "icinga":
    apache_conf              => "icinga.conf.ril",
    execute_service_checks   => "1", # disable
    execute_host_checks      => "1", # disable
    process_performance_data => "0", # disable
    enable_notifications     => "1", # enable
    central                  => "true", # give me all generated configs
  }

  # generate static icinga host and service checks.
  class { "icinga::static::ril": }

  class { "icinga::nsca": }
}
