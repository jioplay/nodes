node "base-sqdtb" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "database-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
   }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Database",
        hosttype    => "VM",
        stage       => runtime,
    }

    class { "os::hosts":
        stage => setup,
        local_hosts => [ "localhost.localdomain",
                         "localhost",
                        ],
    }

    file { ["/var/log/mysql"]:
        ensure  => directory,
        owner   => mysql,
        group   => mysql,
        mode    => "0755",
     }

    class { "mysql":
        icinga_instance => "icinga.mum1.r4g.com",
        mysql_package_ensure => '5.1.69-1.el6_4',
        mysql_server_package_ensure => '5.1.69-1.el6_4',
        install_package_only => true,
    }

    $server_id = $::hostname ? {
        sqdtb01p1 => '1',
        sqdtb02p1 => '2',
    }

    $mysql_backup = $::hostname ? {
        sqdtb01p1 => false,
        sqdtb02p1 => true,
    }

    mysql::instance { "default_instance":
        default_instance => true,
        mysql_port => '3306',
        mysql_binlog_format => 'STATEMENT',
        server_id => $server_id,
        mysql_backup => $mysql_backup,
        require => [Class['mysql'],],
    }

    $default_socket = "/var/lib/mysql/mysql.sock"
        mysql::instance { "third_party_instance":
        default_instance => false,
        mysql_identifier => 'third_party',
        mysql_port => '3307',
        mysql_binlog_format => 'MIXED',
        server_id => $server_id,
        mysql_backup => $mysql_backup,
        require => [Class['mysql'],],
    }

    $third_party_socket = "/var/lib/mysql_third_party/mysql.sock"
    class {"mysql::replication":
        require => [Class['mysql'],
           Mysql::Instance["default_instance"],
           Mysql::Instance["third_party_instance"],
        ],
    }

    class { "keepalived":
        multiple_vrrp_instances => true,
        notify_email => ["ril-support@mobitv.com","dba@mobitv.com","Sanjay.Kuma.Singh@ril.com"],
        notify_from => "keepalived.status@rjil.net",
        smtp_server      => "10.137.2.23",
    }

    # Dual BACKUP for no preempt configuration
    $master_or_backup = $::hostname ? {
        sqdtb01p1 => 'BACKUP',
        sqdtb02p1 => 'BACKUP',
    }

    $priority = $::hostname ? {
        sqdtb01p1 => '101',
        sqdtb02p1 => '100',
    }

    $no_preempt = $::hostname ? {
        sqdtb01p1 => true,
        sqdtb02p1 => true,
    }

    $hostmanager_bind_ip_arr = dns_a("mydtbvip.${::domain}")
    keepalived::vrrp_instance { 'default_instance':
        instance_name     => "default_instance", # unique to network
        virt_ip           => $hostmanager_bind_ip_arr[0],
        priority          => $priority,
        virtual_router_id => "161",
        master_or_backup  => $master_or_backup,
        no_preempt        => $no_preempt,
        check_script      => "/usr/bin/mysql --socket=${default_socket} --connect_timeout=3 -u${::mysql::icinga_mysql_user} -p${::mysql::icinga_mysql_password} -e \"select version();\"",
        interval          => "2",
        weight            => "0",
        smtp_server       => "10.137.2.23",
    }

    $hostmanagertransient_bind_ip_arr = dns_a("mydtbtransientvip.${::domain}")
    keepalived::vrrp_instance { 'third_party_instance':
        instance_name     => "third_party_instance", # unique to network
        virt_ip           => $hostmanagertransient_bind_ip_arr[0],
        priority          => $priority,
        virtual_router_id => "162",
        master_or_backup  => $master_or_backup,
        no_preempt        => $no_preempt,
        check_script      => "/usr/bin/mysql --socket=${third_party_socket} --connect_timeout=3 -u${::mysql::icinga_mysql_user} -p${::mysql::icinga_mysql_password} -e \"select version();\"",
        interval          => "2",
        weight           => "0",
        smtp_server      => "10.137.2.23",
     }
}

node /^sqdtb01p1.*/ inherits "base-sqdtb" {
 }

node /^sqdtb02p1.*/ inherits "base-sqdtb" {
}

