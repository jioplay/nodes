node "ril-prod-stvav" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "vod-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "apache":
        icinga          => false,
        service_ensure  => "stopped",
        service_enable  => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "stvav",
        hosttype    => "vm",
        stage       => runtime,
    }
    varnish::healthcheck { "fmp4_health":
        url         => "/vod/monitoring/health",
        interval    => "5s",
        timeout     => "1s",
        window      => 4,
        threshold   => 3,
        initial     => 3,
        response    => 200,
    }

    varnish::healthcheck { "hls_health":
        url         => "/check.txt",
        interval    => "5s",
        timeout     => "1s",
        window      => 4,
        threshold   => 3,
        initial     => 3,
        response    => 200,
    }

    varnish::pool { "fmp4_vod":
        backends    => ["stfmv01p1.mum1.rjil.com","stfmv02p1.mum1.rjil.com" ],
        port        => "80",
        healthcheck => "fmp4_health",
        method      => "round-robin",
    }

     varnish::pool { "hls_vod":
        backends    => ["stmxv01p1.mum1.rjil.com","stmxv02p1.mum1.rjil.com","stmxv03p1.mum1.rjil.com","stmxv04p1.mum1.rjil.com"],
        port        => "80",
        healthcheck => "hls_health",
        method      => "round-robin",
    }

     varnish::channel_rule { "fmp4_rule":
        channels    => [],
        regex       => "^/(vod|recordings|pls|manifest|thumbnail)/.*",
        pool        => "fmp4_vod",
    }

    varnish::channel_rule { "hls_rule":
        channels    => [],
        regex       => "^/vod_hls.*",
        pool        => "hls_vod",
    }

    varnish::channel_rule { "recording_rule":
        channels    => [],
        regex       => "^/recordings_hls.*",
        pool        => "hls_vod",
    }

    varnish::channel_rule { "fmp4_monitoring":
        channels    => [],
        regex       => "^/(vod|recordings|pls|manifest)/monitoring/health$",
        pool        => "fmp4_vod",
    }

    class { "varnish" :
        package                     => "varnish-3.0.4-1.el6",
        package_ensure              => "present",
        configs                     => ["ril/haproxy-fmp4-cache.vcl","ril/haproxy-catchup-fmp4-cache.vcl","ril/hls-limit.vcl","ril/hnap.vcl"],
        varnish_storage_size        => "2G",
        varnish_listen_port         => "8080",
        default_backend_port        => "8080",
        require                     => [
        Varnish::Channel_rule["recording_rule"],
        Varnish::Channel_rule["hls_rule"],
        Varnish::Channel_rule["fmp4_rule"],
        Varnish::Channel_rule["fmp4_monitoring"],
        Varnish::Healthcheck["fmp4_health"],
        Varnish::Healthcheck["hls_health"],
        Varnish::Pool["fmp4_vod"],
        Varnish::Pool["hls_vod"],
        ],
    }
}

node /^stvav\d{2}p1\.mum1\.rjil\.com$/ inherits "ril-prod-stvav" {

    class { "haproxy":
        config_template => "haproxy_varnish_load.cfg.erb",
        bind_port       => "80",
        backend_port    => "8080",
        balance_list    => ["stvav01p1.mum1.rjil.com, stvav02p1.mum1.rjil.com, stvav03p1.mum1.rjil.com, stvav04p1.mum1.rjil.com, stvav05p1.mum1.rjil.com, stvav06p1.mum1.rjil.com, stvav07p1.mum1.rjil.com, stvav08p1.mum1.rjil.com, stvav09p1.mum1.rjil.com, stvav10p1.mum1.rjil.com, stvav11p1.mum1.rjil.com, stvav12p1.mum1.rjil.com, stvav13p1.mum1.rjil.com, stvav14p1.mum1.rjil.com, stvav15p1.mum1.rjil.com, stvav16p1.mum1.rjil.com, stvav17p1.mum1.rjil.com, stvav18p1.mum1.rjil.com, stvav19p1.mum1.rjil.com, stvav20p1.mum1.rjil.com, stvav21p1.mum1.rjil.com, stvav22p1.mum1.rjil.com, stvav23p1.mum1.rjil.com, stvav24p1.mum1.rjil.com, stvav25p1.mum1.rjil.com, stvav26p1.mum1.rjil.com, stvav27p1.mum1.rjil.com, stvav28p1.mum1.rjil.com, stvav29p1.mum1.rjil.com, stvav30p1.mum1.rjil.com, stvav31p1.mum1.rjil.com, stvav32p1.mum1.rjil.com, stvav33p1.mum1.rjil.com, stvav34p1.mum1.rjil.com, stvav35p1.mum1.rjil.com, stvav36p1.mum1.rjil.com, stvav37p1.mum1.rjil.com, stvav38p1.mum1.rjil.com, stvav39p1.mum1.rjil.com, stvav40p1.mum1.rjil.com, stvav41p1.mum1.rjil.com, stvav42p1.mum1.rjil.com, stvav43p1.mum1.rjil.com, stvav44p1.mum1.rjil.com, stvav45p1.mum1.rjil.com, stvav46p1.mum1.rjil.com, stvav47p1.mum1.rjil.com, stvav48p1.mum1.rjil.com, stvav49p1.mum1.rjil.com, stvav50p1.mum1.rjil.com, stvav51p1.mum1.rjil.com, stvav52p1.mum1.rjil.com, stvav53p1.mum1.rjil.com, stvav54p1.mum1.rjil.com, stvav55p1.mum1.rjil.com, stvav56p1.mum1.rjil.com, stvav57p1.mum1.rjil.com, stvav58p1.mum1.rjil.com"],
        manage_package  => "1.4.22-5.el6_4",
    }

}