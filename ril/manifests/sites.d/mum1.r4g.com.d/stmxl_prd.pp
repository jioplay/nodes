node "ril-prod-stmxl" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Live-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "stmxl",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache":}
        package { "mobi-hls-playlists-live-ril-v2":
        ensure => "5.5.0-201502060704.ed30668",
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_PHONE.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '632000'},
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '1264000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_PHONE_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_PHONE_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '632000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_PHONE_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '1264000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_TABLET.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '632000'},
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '1264000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
            {'variant'=>'H13_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '2428000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_TABLET_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_TABLET_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '632000'},
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '1264000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/HD_TABLET_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H13_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '2428000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_PHONE.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '296000'},
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '664000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_PHONE_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '296000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_PHONE_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_PHONE_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '664000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_TABLET.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '296000'},
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '664000'},
            {'variant'=>'H8_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '1264000'},
            {'variant'=>'H13_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_TABLET_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '296000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_TABLET_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '432000'},
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '664000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/live_hls/SD_TABLET_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H8_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '1264000'},
            {'variant'=>'H13_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"]
    }

}

node /^stmxl\d{2}p1\.mum1\.rjil\.com$/ inherits "ril-prod-stmxl" {

    class { "mobi_mediamuxer":
        version => "0.7.3-192",
        livebackend_fragmentserver => "http://fragserverlivevip/live",
        vodbackend_fragmentserver =>  undef,
        recordingbackend_fragmentserver => undef,
        licensemanagerserver => "http://drmvip:8080",
        require => Class["apache"],
    }

}

