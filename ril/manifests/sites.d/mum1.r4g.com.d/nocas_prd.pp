node "ril-prod-nocas" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "database-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "nocas",
        hosttype    => "vm",
        stage       => runtime,
    }

    
     class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-211717",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
        initheapsize       => "1024",
        heapsize           => "4096",
        permgensize        => "128",
    }

    class { "mobi_storage_manager":
        mobi_storage_manager_version => "5.5.0-272875",
        notify                       => Class["tomcat::service"],
        require                      => Class["tomcat"],
    }
}
node "nocas01p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "0",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}
node "nocas02p1.mum1.r4g.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "56713727820156410577229101238628035242",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}
node "nocas03p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas04p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas05p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas06p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas07p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas08p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas09p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas10p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas11p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas12p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas13p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas14p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas15p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas16p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}

node "nocas17p1.mum1.rjil.com" inherits "ril-prod-nocas" {

    class { "mobi_cassandra":
        cassandra_version => "1.1.5-272253",
        cluster_name      => "Reliance Cluster",
        seeds             => "nocas01p1,nocas02p1,nocas03p1,nocas04p1,nocas05p1,nocas06p1,nocas07p1,nocas08p1,nocas09p1,nocas10p1,nocas11p1,nocas12p1,nocas13p1,nocas14p1,nocas15p1,nocas16p1,nocas17p1",
        initial_token     => "113427455640312821154458202477256070484",
        auto_bootstrap    => true,
        require           => Class["java"],
    }

    package { "mobi-pi-jre":
        ensure  => "1.6.0-201402142337.3424cd1",
        require => Class["java"],
    }

    package { "java-1.6.0-openjdk-1.6.0.0-1.65.1.11.13.el6_4":
        ensure => "absent"
    }
}
