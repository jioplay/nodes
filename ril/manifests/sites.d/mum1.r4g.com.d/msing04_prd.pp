node "msing04p1.mum1.r4g.com" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "epg-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "Ingestor",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache":
            package => "httpd.x86_64",
    }

     class { "java" :
            jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
            jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-211717",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        jars                    => [ "c3p0-0.9.2-pre2.jar", "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
	heapsize	   => "2048",
	permgensize	   => "128", 
    }

    class { "mobi_program_scheduler":
             ril                                => true,
             package_ensure                     => "5.3.1-201410100824.4a250fc",
             vid_carrier                        => "infotel",
             vid_product                        => "r4g",
             vid_version                        => "5.0",
             vid_guideprovider                  => "default",
             solr_server_url                    => "http://solrslavevip:8080/mobi-solr/",
             notification_programstart_enabled  => "true",
             notification_preauthcheck_enabled  => "false",
             notification_preauthcheck_interval => "20",
             notification_programstart_cron     => "0 0/5 * * * ?",
             notification_programstart_interval => "5",
	     notification_housekeepingshared_cron => "0 15 10 31 12 ? 2099",
             guide_manager_url                  => "http://guidemanagervip:8080/mobi-guide-manager/guide/v5/lineup",
             db_username                        => "program_user",
             db_password                        => "Kko_43975DFgnow",
             hibernate_database_dialect         => "org.hibernate.dialect.MySQLDialect",
             jdbc_driver                        => "com.mysql.jdbc.Driver",
             jdbc_url                           => "jdbc:mysql://mydtbtransientvip:3307/program_scheduler",
             program_queue_eit_messages_name    => "PROGRAM.EIT_MESSAGES.QUEUE",
             consumer_queue_program_eit_messages_size => "1",
             notify                             => Class["tomcat::service"],
             require                            => Class["tomcat"],
    }

     class { "nfs":
        client_ensure => true,
        client_enable => true,
        #client_mounts => { 'new_ril_prod' => '/var/Jukebox' },
        client_mounts => { 'ril_trial' => '/var/Jukebox' },
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }

    class { "mobi_epg_ingestor":
        package                 => "mobi-epg-ingestor-5.3.90-201504280609.52ca0bd",
        solr_server_url         => "http://solrmastervip:8080/mobi-solr/",
        solr_archive_server_url => "http://archivesolrmastervip:8080/mobi-solr/",
        partner_xsl_file        => "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/xslt/",
        partner_stage_folder    => "/var/data/mobi-publisher-programdata/partner",
        tms_stage_folder        => "/var/data/mobi-publisher-programdata/tms",
        ingestor_enabled        => "false",
        source_data_file        => "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/data/xmltv_terrestrial.xml",
        xsl_file                => "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/xslt/mobi_data.xsl",
        channel_data_file       => "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/data/",
        config_folder           => "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes",
        buffer                  => "86400",
        master                  => "Regular",
        ccr                     => "Change",
        rtcr                    => "Change",
        archive_enabled         => "true",
        notify                  => Class["tomcat::service"],
        require                 => [ Class["tomcat"], ],
    }

    class { "mobi_ril_dummy_epg_generator":
        package_ensure          => "5.3.0-201407310654.8abd788",
        dummy_epg_generator_cron_expression => "0 0 1,12 * * ?",
        channel_ids_program_name => "180,182,184,272,276,277,278",
        notify                  => Class["tomcat::service"],
        require                 => [ Class["tomcat"], ],
    }

    class { "mobi_epg_preprocessor":
        package_ensure          => "1.1.1-201408120825.9c0db76",
    }

    class { "mobi_ril_recording_generator":
        package_ensure          => "5.3.0-276978",
        recording_generator_job_enabled => "false",
        recording_channel_ids    => "5,10,15,20,25,267,268,269,270,271",
    }

    class { "mobi_ril_recommendation_adapter":
       package_ensure => "5.3.0-201408180655.87cd646",
       require => [Class["tomcat"],Class["nfs"],File["/var/Jukebox/ftpupload/Recommendation"],],
       auth_manager_endpoint_url	=> "http://authmanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
       identity_manager_endpoint_url	=> "http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
       recommendation_cron_expression	=> "0 10 0,8 * * ?",
       recommendation_csv_file_path	=> "/var/Jukebox/ftpupload/Recommendation",
       series_ids_file_path		=> "/var/Jukebox/ftpupload/Recommendation/series_thumbnail.txt",
       default_no_of_recommendation	=> "50",
       max_allowed_channel_ids		=> "100",
      }

    class { "mobi_catchup_post_processor" :
       package => "mobi-catchup-post-processor",
       version => "5.3.0-201410170822.d8f3a0a",
       solr_server_url => "http://solrmastervip:8080/mobi-solr/",
       amq_broker_url => "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100&startupMaxReconnectAttempts=10",
       core_pool_size => "150",
       max_pool_size => "200",
       task_queue_capacity => "1000",
       consumer_queue_catchup_post_processor_name => "CATCHUP.POST.PROCESSOR.QUEUE",
       catchup_queue_concurrent_consumers => "1",
       catchup_recording_base_location => "/var/Jukebox/recordings/",
       catchup_recording_file_suffix => "mp4",
       atomhopper_url => "http://atomhoppervip:8080/mobi-thirdparty-atomhopper/atom/program_feed/",
       connection_timeout => "3000",
       read_timeout => "3000",
    }


    file { "/var/Jukebox/ftpupload/Recommendation":
	ensure  => directory,
	owner   => "apache",
	group   => "arlanda",
	mode    => "0775",
	require => Class["nfs"]
    }

    file { "mobi-epg-ingestor-nightly_passive":
	path	=> "/etc/cron.d/mobi-epg-ingestor-nightly",
        source	=> "puppet:///modules/mobi_epg_ingestor/ril/mobi-epg-ingestor-nightly_active",
	ensure  => present,
	owner   => "root",
	group   => "root",
	mode    => "0644",
    }

    file { [ "/opt/ReachTV/", "/opt/ReachTV/Farm/", "/opt/ReachTV/Farm/monitor" ]:
	ensure => "directory",
	owner   => "root",
	group   => "root",
	mode    => "0777",
    }

    file { [ "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/data/EPGData" ]:
	ensure => "directory",
	owner   => "root",
	group   => "root",
	mode    => "0755",
	require => Class ["mobi_epg_ingestor"],
    }

    packages::realize_package{["git"]:}

#    vcsrepo { "ingestor.git":
#	path => "/var/Jukebox/appdata/ingestor",
#        ensure => latest,
#        provider => git,
#        source => 'git://ingit01p1.mum1.r4g.com/ingestor.git',
#        revision => 'ril-prod',
#        require => [Package["git"],Class["mobi_epg_ingestor"],],
#    }

    #mobi_epg_ingestor::ril::create_static_file{ "update_solr":
    #    file_path   => "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/data/channel_reliance.xml",
    #    file_source => "puppet:///modules/mobi_epg_ingestor/ril/channel_reliance.xml",
    #    mode        => "777",
    #    owner       => "rtv",
    #    group       => "rtv",
    #    ensure      => "present",
     #   #command     => "/usr/bin/curl -i http://localhost:8080/mobi-epg-ingestor/v5/ingestor/convertAndIngestChannelXML",
    #}
}
