node ril-prod-nvrcw {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "recording-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
        manage_rsyslog   => false,
        manage_syslog_ng => true,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "nvrcw",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "java":
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    # need class nfs here with the ril config...
    class { "nfs":
        client_ensure  => true,
        client_enable  => true,
        client_mounts  => { 'ril_prod' => '/var/Jukebox' },
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }

   package { "mobi-user-rtv-1.2.3-79211":
       ensure => present,
   }

}

node "nvrcw01p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw02p1.mum1.rjil.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw03p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw04p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw05p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw06p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw07p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw08p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw09p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

node "nvrcw10p1.mum1.r4g.com" inherits "ril-prod-nvrcw" {
     class { "mobi_recordingwriter" :
        rw_recordingmaster_vip   => "recordingmastervip",
        rw_recordingmaster_port  => "8080",
        rw_recordingmaster_uri   => "record_master",
        version                  => "0.8.0-748",
        sku_template             => "LIVEPROGRAM_CHANNEL_\${channel id}",
        fragcontroller_url       => "recordingmastervip",
        fragcontroller_rest_port => "8080",
        max_job_age_sec          => "370",
        frag_cache_size_sec      => "24",
        frag_cache_workers       => "20",
        shard_amq_broker_hosts   => ["nvamq01","nvamq02",],
    }
}

