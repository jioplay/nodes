#node /^msofm\d{2}p1\.mum1\.rjil\.com$/ {
node "ril-msofm" {
#Reconciliation Version (Final) - working

    class { "base" :
        host_type               => "qa",
        manage_puppet_config    => true,
        manage_ganglia          => true,
        manage_icinga           => true,
       gmond_cluster_name => "Auth-Subscriptions-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        manage_mcollective      => false,
    }

    class { "os::motd" :
        owner       => "Pinda Ndaki",
        owneremail  => "pndaki@mobitv.com",
        stack       => "ril-prod",
        servertype  => [" Offer mgmt,",
                        " payment adapter,",
                        " 3rd party,",
                        ],
        hosttype    => "VM",
        stage       => runtime,
    }

    class { "os::hosts":
        stage => setup,
        local_hosts => [ "localhost.localdomain",
                         "localhost",
                         "msofm01p1.ci-ril.dev.smf1.mobitv msofm01p1",
                        ],
    }

    class { "java" :
            jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
            jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-209592",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
     initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
    }

    class { 'mobi_push_dispatcher':
        mobi_push_dispatcher_package => "mobi-push-dispatcher-5.5.0-271745",
        dispatcher_host_name         => $::fqdn,
        xmpp_domain_name             => 'xmpp.ril.com',
        require                      => Class["mobi_openfire"],
    }

    class { 'mobi_openfire':
        mobi_openfire_package        => 'mobi-openfire-3.8.0-271807',
        mobi_openfire_package_ensure => 'present',
        xmpp_domain_name             => 'xmpp.ril.com',
        admin_email                  => 'bhushan_shelke@persistent.co.in',
        memcached_servers => 'msliv01:11211',

    }


    class { 'mobi_openfire_external_authentication':
        package        => 'mobi-openfire-external-authentication-5.4.0-265834',
        package_ensure => 'present',
        require        => Class["mobi_openfire"],
    }

    class {'mobi_openfire_external_presence_plugin':
        mobi_openfire_external_presence_plugin_package        => 'mobi-openfire-external-presence-plugin-3.8.0-265834',
        mobi_openfire_external_presence_plugin_package_ensure => present,
        require                                               => Class["mobi_openfire"],
    }

    class {'mobi_openfire_websocket_plugin':
          mobi_openfire_websocket_plugin_package => 'mobi-openfire-websocket-plugin-3.8.0-265834',
          mobi_openfire_websocket_plugin_package_ensure => 'present',
          require => Class["mobi_openfire"],
    }
}
node "msofm02p1.mum1.rjil.com" inherits "ril-msofm" {
    class { "mobi_aaa_reliance_offer_manager":
        #mobi_aaa_reliance_offer_manager_version => "5.3.91-272023",
        mobi_aaa_reliance_offer_manager_version => "5.3.92-201407070459.c6371d2",
        external_offer_metadata_xml_file_path => "/opt/mobi-aaa-ril-offer-manager/catalog_xml/new/",
        external_offer_metadata_job_enabled => "false",
        external_offer_metadata_cron_expression => "0 0/5 * * * ?",
        notify => Class["tomcat::service"],
        require => Class["tomcat"],
    }

}

node "msofm01p1.mum1.rjil.com" inherits "ril-msofm" {
    class { "mobi_aaa_reliance_offer_manager":
        #mobi_aaa_reliance_offer_manager_version => "5.3.91-272023",
        mobi_aaa_reliance_offer_manager_version => "5.3.92-201407070459.c6371d2",
        external_offer_metadata_xml_file_path => "/opt/mobi-aaa-ril-offer-manager/catalog_xml/new/",
        external_offer_metadata_job_enabled => "true",
        external_offer_metadata_cron_expression => "0 0/5 * * * ?",
        notify => Class["tomcat::service"],
        require => Class["tomcat"],
    }

}
