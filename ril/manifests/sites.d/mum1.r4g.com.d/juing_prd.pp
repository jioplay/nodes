node /^juing\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "cms-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Jukebox Ingestor",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache":
        package => "httpd.x86_64",
    }

    class { "php": }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "mobi_cms_us::tomcat_config":
        tomcat_umask => "0000",
    }

    class { "nfs":
        client_ensure  => true,
        client_enable  => true,
        client_mounts  => { 'ril_prod' => '/var/Jukebox' },
        client_rsize   => "131072",
        client_wsize   => "524288",
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }

     class { "tomcat":
        package => "mobi-tomcat-config-8080",
        version => "6.0.35-211717",
        override_catalina => false,
        override_server_config => false,
        config_owner => "rtv",
        config_group => "rtv",
        jars => [ "hsqldb-1.8.0.7.jar", "c3p0-0.9.2-pre2.jar", "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        initscript => "tomcat8080",
        require => Class["java"],
        jrehome => "",
    }

    class { "mobi_arlanda_ftplogparser":
        version => "5.4.0-267007",
        jdbc_driver => "com.mysql.jdbc.Driver",
        ftplogparser_db_jdbc_url => "jdbc:mysql://mydtbvip:3306/dam?autoReconnect=true",
        db_username => "dam_user",
        db_password => "n5_2_djTsa,vcdj",
        require => [ Class["java"], Class["nfs"], Class["tomcat"] ],
        vsftpd_local_auth => true,
    }

    class { "mobi_arlanda_ingestor":
        version => "5.4.0-267001",
        ril     => true,
        dam_datasource        => true,
        offer_management_url  => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager/guide/v5/",
        subconvertor_location => "/usr/local/ods/mediatools/bin/subconverter",
        subconvertor_timeout  => "3000",
        ing_db_username       => "ing_user",
        ing_db_password       => "mlo_do90dflflKG4",
        dam_db_username       => "dam_user",
        dam_db_password       => "n5_2_djTsa,vcdj",
        folder_manager_rest_url  => "http://juarnvip:8080/dam/",
        offer_management_vod_url => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager/guide/v5/asset/vod/{MID}.json?includeClipChanels=true",
        offer_management_episode_url => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager/guide/v5/asset/episode/{MID}.json",
        offer_management_series_url  => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager/guide/v5/asset/series/{MID}.json",
        policy_management_url  => "http://policymanagervip:8080/mobi-policy-manager/services",
        policy_manager_timeout => "30",
        policy_manager_url => "http://policymanagervip:8080/mobi-policy-manager/services",
        notify  => [ Class["tomcat::service"], Class["apache::service"] ],
        require => [ Class["nfs"], Class["apache"], Class["tomcat"], Class["php"], ],
        asset_resource_file_handling => "copy",
    }

    class { "mobi_arlanda_searchagent":
        version => "5.4.0-266994",
        require => [Class["tomcat"],Class["java"], ] ,
        update_thumbnail_url => "http://damvip:8080/dam/services/thumbnails/vodThumbnail/1024/768/",
    }

     package { "mobi-live-channel-config-5.0.0.5-206267":
        require => [ Class["mobi_arlanda_searchagent"], Class["mobi_arlanda_ingestor"], Class["mobi_arlanda_ftplogparser"],],
    }

    class { "mobi_arlanda_star_ingestor":
        version => "5.4.0-253943",
        require => Class["java"],
    }

    class { "mobi_mediatools":
        version => "0.6.1-266",
    }

    class { "mobi_livetovod_adapter":
        livetovod_version => "5.3.0-274333",
        livetovod_job_enabled => "true",
        recording_manager_profile_id => "OTE1MDkjdmlkZW8ub3BlcmF0b3IjYzliMjJjZGYtYzRkMS00NzlkLWE3ZjYtYTQ1OTQ3Y2M5MDAy",
    }

   realize(Accounts::Group["infotel"], Accounts::Group["yume"], Accounts::Group["star"],Accounts::Group["indiacast"],Accounts::Group["indiacast_tvshow"], Accounts::Group["rajshri"],Accounts::Group["mobi_test"],)

   realize(Accounts::User["infotel"], Accounts::User["yume"], Accounts::User["star"],Accounts::User["indiacast"],Accounts::User["indiacast_tvshow"], Accounts::User["rajshri"],Accounts::User["mobi_test"],)

}
