node /^msath\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Auth-Subscriptions-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "AAA",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "os::hosts":
        stage => setup,
        remote_hosts => [ "10.135.140.184 api-preprod.ril.com","10.66.4.154 rssg04.ril.com","10.66.4.151 apis.ril.com" ],
    }

    class { "apache":
            package => "httpd.x86_64",
    }

    class { "java":
            jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
            jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-211717",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        jars => [ "c3p0-0.9.2-pre2.jar", "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
    }

    class { "mobi_aaa_reliance_identity_manager":
        #For IDAM 3.X
        mobi_aaa_reliance_identity_manager_version => "5.4.0-201506120649.5c67814",
        reliance_api_key => "l7xx0d9c3da51d7548abacceba8ff512427b",
        reliance_openssl_url => "https://api-preprod.ril.com:8443",
        access_token_dynamic_attributes => "mail",
        #For IDAM 2.X
        #mobi_aaa_reliance_identity_manager_version => "5.4.0-275407",
        #reliance_api_key => "l7xx03ee98e0ecb1433da2a5973744cda017",
        #reliance_openssl_url => "https://rssg04.ril.com:8443",
        #access_token_dynamic_attributes => "mail",
        connection_timeout_secs => "10",
        rpsl_pin_expirytime_milliseconds => "450000",
        profile_dynamic_attributes => "gender,dob,city,mail,telecomCircle,postalCode,preferredLanguage,mobile,uid,unique,subscriberId",
        httpconnection_pool_size => "4500",
        ril_idam_memcached_servers => "msliv01p1.mum1.rjil.com:11211,msliv02p1.mum1.rjil.com:11211,msliv03p1.mum1.rjil.com:11211,msliv04p1.mum1.rjil.com:11211,msliv05p1.mum1.rjil.com:11211,msliv06p1.mum1.rjil.com:11211,msliv07p1.mum1.rjil.com:11211,msliv08p1.mum1.rjil.com:11211,msliv09p1.mum1.rjil.com:11211,msliv10p1.mum1.rjil.com:11211,msliv11p1.mum1.rjil.com:11211,msliv12p1.mum1.rjil.com:11211,msliv13p1.mum1.rjil.com:11211,msliv14p1.mum1.rjil.com:11211,msliv15p1.mum1.rjil.com:11211,msliv16p1.mum1.rjil.com:11211,msliv17p1.mum1.rjil.com:11211,msliv18p1.mum1.rjil.com:11211,msliv19p1.mum1.rjil.com:11211,msliv20p1.mum1.rjil.com:11211,msliv21p1.mum1.rjil.com:11211,msliv22p1.mum1.rjil.com:11211,msliv23p1.mum1.rjil.com:11211,msliv24p1.mum1.rjil.com:11211,msliv25p1.mum1.rjil.com:11211,msliv26p1.mum1.rjil.com:11211,msliv27p1.mum1.rjil.com:11211,msliv28p1.mum1.rjil.com:11211,msliv29p1.mum1.rjil.com:11211,msliv30p1.mum1.rjil.com:11211,msliv31p1.mum1.rjil.com:11211,msliv32p1.mum1.rjil.com:11211,msliv33p1.mum1.rjil.com:11211,msliv34p1.mum1.rjil.com:11211,msliv35p1.mum1.rjil.com:11211,msliv36p1.mum1.rjil.com:11211,msliv37p1.mum1.rjil.com:11211,msliv38p1.mum1.rjil.com:11211,msliv39p1.mum1.rjil.com:11211,msliv40p1.mum1.rjil.com:11211,msliv41p1.mum1.rjil.com:11211,msliv42p1.mum1.rjil.com:11211,msliv43p1.mum1.rjil.com:11211,msliv44p1.mum1.rjil.com:11211,msliv45p1.mum1.rjil.com:11211,msliv46p1.mum1.rjil.com:11211,msliv47p1.mum1.rjil.com:11211,msliv48p1.mum1.rjil.com:11211,msliv49p1.mum1.rjil.com:11211,msliv50p1.mum1.rjil.com:11211,msliv51p1.mum1.rjil.com:11211,msliv52p1.mum1.rjil.com:11211,msliv53p1.mum1.rjil.com:11211,msliv54p1.mum1.rjil.com:11211,msliv55p1.mum1.rjil.com:11211,msliv56p1.mum1.rjil.com:11211,msliv57p1.mum1.rjil.com:11211,msliv58p1.mum1.rjil.com:11211,msliv59p1.mum1.rjil.com:11211,msliv60p1.mum1.rjil.com:11211",
        ril_idam_memcached_access_timeout => "3000",
        ril_idam_memcached_access_expireTime =>"86400",
        method_execution_time => "200",
        access_token_lifetime_secs => "86400",
        notify => Class["tomcat::service"],
        require => Class["tomcat"],
    }

    class { "mobi_aaa_ril_identity_manager_stub":
            mobi_aaa_ril_identity_manager_stub_version => "5.3.0-260571",
            notify                                     => Class["tomcat::Service"],
            require                                    => Class["tomcat"],
    }

    class { "mobi_aaa_rights_manager":
          package_name => "mobi-aaa-ril-rights-manager",
          rights_manager_version => "5.5.0-201502030533.f92791d",
          solr_server_url => "http://solrmastervip:8080/mobi-solr/",
          jerseyclient_enable_clientlog => "true",
          policy_manager_url => "http://policymanagervip:8080/mobi-policy-manager",
          oms_url => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager",
          am_url => "http://accountmanagementvip:8080/mobi-aaa-ril-identity-manager-oauth2",
          ### MySQL ###
          database_driver => "com.mysql.jdbc.Driver",
          hibernate_dialect => "org.hibernate.dialect.MySQL5Dialect",
          database_url => "jdbc:mysql://mydtbvip:3306/rights_manager",
          database_username => "rights_user",
          database_password => "kl_rkl68P_GJfdf",
          end_date_grace_period => "15",
          core_service_rights_mobitv_reference => "default",
          httpconnection_pool_size => "2000",
          require =>  Class["tomcat"],
          notify => Class["tomcat::service"],
    }
}
