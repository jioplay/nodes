node /^msepg\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "epg-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Episode Guide",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache" :
       package => "httpd.x86_64",
    }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-211717",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
        heapsize           => "2048",
        permgensize        => "128",
    }

    class { "mobi_guide_manager":
        mobi_guide_manager_version => "5.5.1-201504291127.aea5f07",
        carrier                    => "infotel",
        solr_max_rows              => "1000",
        notify                     => [ Class["tomcat::service"], Class["apache::service"] ],
        require                    => [ Class["tomcat"], Class["apache"] ],
    }

    file { "solr_field.csv":
        path    => "/opt/mobi-guide-manager/config/solr_field.csv",
        source  => "puppet:///modules/mobi_guide_manager/solr/solr_field.csv",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0755",
        require => Class["mobi_guide_manager"],
    }

    file { "schema.xml":
        path    => "/opt/mobitv/solr/conf/schema.xml",
        source  => "puppet:///modules/mobi_guide_manager/solr/schema.xml",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
        require => Class["mobi_guide_manager"],
    }

    file { "synonyms.txt":
        path    => "/opt/mobitv/solr/conf/synonyms.txt",
        source  => "puppet:///modules/mobi_guide_manager/solr/synonyms.txt",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
        require => Class["mobi_guide_manager"],
    }

    file { "wordDelimiterTypes.txt":
        path    => "/opt/mobitv/solr/conf/wordDelimiterTypes.txt",
        source  => "puppet:///modules/mobi_guide_manager/solr/wordDelimiterTypes.txt",
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
        require => Class["mobi_guide_manager"],
    }

    class { "solr":
        mode        => "slave",
        version     => "5.0.0-220570",
        common_ver  => "5.2.0-221168",
        notify      => Class["tomcat::service"],
        require     => Class["tomcat"],
    }

    class { "mobi_config_manager":
        mobi_config_manager_version => "5.0.0-209622",
        mobi_publisher_endpoint_version => "5.0.0-197561",
        notify => [Class["tomcat::service"]],
        require => [Class["tomcat"]],
    }

    class { "mobi_catalogue_manager":
        ipaddress => "49.40.0.18",
        carrier => "infotel",
        product => "r4g",
        version => "5.0",
        fb_appid => "450893638306989",
        social_live_program => "4_7467358",
        social_live_channel => "4",
        social_linear_epg => "4_7467358",
        social_vod_url => "4896",
        package_ensure => "5.4.0-201503180817.7e6b69d",
        notify => [ Class["tomcat::service"], Class["apache::service"] ],
        require => [ Class["tomcat"], Class["apache"] ],
    }

    package { "mobi-guide-manager-extension-ril":
       ensure => "5.5.0-201407141046.456b165",
       require => Class["mobi_guide_manager"],
       notify => [Class["tomcat::service"], Class["apache::service"]],
    }

    class { "mobi_thirdparty_atomhopper":
       package => "mobi-thirdparty-atomhopper",
       database_url => "jdbc:mysql://mydtbvip:3306/atomhopper",
       version => "5.4.1-201410170828.9b69ed0",
    }

    packages::realize_package{["git"]:}

    #vcsrepo { "/var/www/publisher/configmanager/current/":
    #    ensure => latest,
    #    provider => git,
    #    source => 'git://ingit01p1.mum1.rjil.com/config-manager.git',
    #    revision => 'ril-prod',
    #    require => [Package["git"],Class["mobi_config_manager"],],
    #}

    #vcsrepo { "/var/www/publisher/icon/current/":
    #    ensure => latest,
    #    provider => git,
    #    source => 'git://ingit01p1.mum1.rjil.com/icons.git',
    #    revision => 'ril-prod',
    #    require => [Package["git"],Class["mobi_config_manager"],],
    #}
}
