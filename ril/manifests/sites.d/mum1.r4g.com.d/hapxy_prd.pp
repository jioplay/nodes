node /^hapxy\d{2}p1\.mum1\.r4g\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Admin-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "HAProxy Load Balancer",
        hosttype    => "vm",
        stage       => runtime,
    }

}
