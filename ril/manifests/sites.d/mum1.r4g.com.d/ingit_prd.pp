node /^ingit\d{2}p1\.mum1\.r4g\.com$/ {
    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Admin-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "Ingestor",
        hosttype    => "vm",
        stage       => runtime,
    }

    $gitserver_user = 'gitolite'
    class { 'gitserver':
      perl_time_hires_package_ensure => '1.9721-131.el6_4',
      gitserver_user     => $gitserver_user,
      gitolite_user      => $gitolite_user,
      gitserver_home_dir => "/home/${gitserver_user}",
      gitolite_ssh_key   => 'ril/ril-git.pub',
    }
}
