node "ril-prod-stmxv" {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "vod-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "Ingestor",
        hosttype    => "vm",
        stage       => runtime,
    }
    class { "apache":}

    package { "mobi-hls-playlists-vod-ril-v2":
        ensure => "0.4.3-1415",
    }

    package { "mobi-httpd-hls-recordings-netpvr-config":
        ensure => "5.5.0-201502131544.b14fa98",
    }

    package { "mobi-hls-playlists-recordings-ril-v2":
        ensure => "5.5.0-201502110952.c2aad7b",
    }

    class { "nfs":
        client_ensure  => true,
        client_enable  => true,
        client_mounts  => { 'ril_prod' => '/var/Jukebox' },
        client_rsize   => "131072",
        client_wsize   => "524288",
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_PHONE.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '864000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_PHONE_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_PHONE_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_PHONE_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '864000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_TABLET.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '864000'},
            {'variant'=>'H13_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_TABLET_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_TABLET_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_W240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
            {'variant'=>'H5_W270_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
            {'variant'=>'H8_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '864000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/HD_TABLET_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H13_W360_H264_MP', 'program_id' => '1', 'bandwidth' => '2428000'},
        ],
        require => Class["apache"],
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_PHONE.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_PHONE_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_PHONE_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_PHONE_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_TABLET.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
            {'variant'=>'H8_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '864000'},
            {'variant'=>'H13_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_TABLET_LOW.m3u8":
        playlist_array => [
            {'variant'=>'H2_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '232000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_TABLET_MEDIUM.m3u8":
        playlist_array => [
            {'variant'=>'H3_S240_H264_MP', 'program_id' => '1', 'bandwidth' => '332000'},
            {'variant'=>'H5_S360_H264_MP', 'program_id' => '1', 'bandwidth' => '548000'},
        ],
        require => Class["apache"]
    }

    mobi_frag_hls::generate_hls_playlist{ "/var/www/playlists/recordings_hls/SD_TABLET_HIGH.m3u8":
        playlist_array => [
            {'variant'=>'H8_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '864000'},
            {'variant'=>'H13_S480_H264_MP', 'program_id' => '1', 'bandwidth' => '1596000'},
        ],
        require => Class["apache"]
    }

}

node /^stmxv\d{2}p1\.mum1\.rjil\.com$/ inherits "ril-prod-stmxv" {

    class { "mobi_mediamuxer":
        version => "0.7.3-192",
        livebackend_fragmentserver => undef,
        vodbackend_fragmentserver =>  "http://fragservervodvip/vod",
        recordingbackend_fragmentserver =>  "http://fragservervodvip/recordings",
        licensemanagerserver => "http://drmvip.mum1.rjil.com:8080",
        require => Class["apache"],
    }

}