node /^msliv\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "others-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Jukebox Arlanda CMS",
        hosttype    => "vm",
        stage       => runtime,
    }
      class { "apache" :
        package => "httpd.x86_64",
    }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package           => "mobi-tomcat-config-8080",
        version           => "6.0.35-241769",
        override_catalina => false,
        server_config_loc => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src => "server.xml.tomcat8080",
        config_owner      => "rtv",
        config_group      => "rtv",
        initscript        => "tomcat8080",
        jrehome           => "",
        require           => Class["java"],
    }
      class { "memcached":
        package     => "memcached",
        version     => "1.4.4",
        srvc_enable => true,
        config      => "memcached.ng",
    }

    class { "mobi_aaa_stream_manager":
        stream_manager_version      => "5.8.0-201502030604.45f3b98",
        auth_oauth2_server_url      => "http://authmanagervip:8080/mobi-aaa-ril-identity-manager-oauth2",
        auth_rights_server_url      => "http://rightsmanagervip:8080/mobi-aaa-rights-manager",
        auth_enable_session_manager => false,
        memcached_servers           => "msliv01p1:11211,msliv02p1:11211,msliv03p1:11211,msliv04p1:11211,msliv05p1:11211,msliv06p1:11211,msliv07p1:11211,msliv08p1:11211,msliv09p1:11211,msliv10p1:11211,msliv11p1:11211,msliv12p1:11211,msliv13p1:11211,msliv14p1:11211,msliv15p1:11211,msliv16p1:11211,msliv17p1:11211,msliv18p1:11211,msliv19p1:11211,msliv20p1:11211,msliv21p1:11211,msliv22p1:11211,msliv23p1:11211,msliv24p1:11211,msliv25p1:11211,msliv26p1:11211,msliv27p1:11211,msliv28p1:11211,msliv29p1:11211,msliv30p1:11211,msliv31p1:11211,msliv32p1:11211,msliv33p1:11211,msliv34p1:11211,msliv35p1:11211,msliv36p1:11211,msliv37p1:11211,msliv38p1:11211,msliv39p1:11211,msliv40p1:11211,msliv41p1:11211,msliv42p1:11211,msliv43p1:11211,msliv44p1:11211,msliv45p1:11211,msliv46p1:11211,msliv47p1:11211,msliv48p1:11211,msliv49p1:11211,msliv50p1:11211,msliv51p1:11211,msliv52p1:11211,msliv53p1:11211,msliv54p1:11211,msliv55p1:11211,msliv56p1:11211,msliv57p1:11211,msliv58p1:11211,msliv59p1:11211,msliv60p1:11211",
        solr_server_url             => "http://solrmastervip:8080/mobi-solr/",
        kafka_logging_enable        => false,
        use_policy_restiction_config     => true,
        jersey_restclient_maxConnections => 3000,
        recording_manager_host      => "recordingmanagervip",
        notify                      => Class["tomcat::service"],
        require                     => Class["tomcat"],
    }

   package { "mobi-user-rtv-1.2.3-79211": 
       ensure => present,
   }


}
