node /^nvrcd\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "recording-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
        manage_rsyslog   => false,
        manage_syslog_ng => true,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "nvrcd",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "java":
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "activemq":
        package         => "mobi-activemq-5.9.0-272600",
        config_template => "activemq.xml.a59.erb",
        config_path     => "/opt/activemq/conf/activemq.xml",
        db_driver       => "com.mysql.jdbc.Driver",
        jdbc_url        => "jdbc:mysql://mydtbtransientvip:3307/netpvr_a",
        db_user         => "activemq_a",
        db_passwd       => "ku9xw_efKJe54vf",
        broker_name     => "netpvr_a",
    }

    file { "/opt/activemq/lib/optional/mysql-connector-java-5.1.19-bin.jar":
        source  => "puppet:///modules/activemq/mysql-connector-java-5.1.19-bin.jar",
        owner   => 'rtv',
        group   => 'rtv',
        require => Class["activemq::install"],
        #notify => Class["activemq::service"],
    }

    class { "mobi_jobdistributor" :
        version                       => "0.7.0-742",
        shard_amq_broker_hosts        => ["nvamq01","nvamq02"],
        jd_shard_identifier           => "shard_a",
        jd_recording_response_timeout => "370",
        jd_recording_stop_timeout     => "5",
        jd_db_engine                  => "mysql",
        jd_db_connect                 => "netpvr_user nsHWM_8dwJ_i38c mydtbtransientvip.mum1.rjil.com:3307 netpvr_a",
        jd_shard_table_name           => "NETPVR_JOBS_A",
        netpvr_request_queue_name     => "NETPVR.REQUESTS?consumer.prefetchSize=0",
        shard_request_channel_name    => "SHARD.REQUESTS?consumer.prefetchSize=0",
        require                       => [ Package["oracle-instantclient-basic-repack-11.2.0.3.0-1"],],
    }

    package { "oracle-instantclient-basic-repack-11.2.0.3.0-1":
         ensure => present,
    }

}
