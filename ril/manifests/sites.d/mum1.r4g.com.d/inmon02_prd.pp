node "inmon02p1.mum1.r4g.com" {

  class { "base":
       host_type               => "ril",
       manage_icinga           => true,
       manage_ganglia          => true,
       puppetagent_version     => "2.7.19-1.el6" ,
       gmond_cluster_name      => "Admin-cluster",
       puppetagent_environment => "qa_ril_5_4_0",
  }

  class { "os::motd" :
       owner       => "MobiTV Operations",
       owneremail  => "ril-support@mobitv.com",
       stack       => "MUM1.R4G",
       servertype  => "Ganglia",
       hosttype    => "vm",
       stage       => runtime,
  }

  class { "apache":
       mpm             => "worker",
       icinga          => true,
       icinga_instance => "inmon01p1.mum1.r4g.com",
  }

  class { "php": }
  class { "ganglia":
       apache_conf   => "ganglia.conf.ril",
       htpasswd_conf => "htpasswd.users.ril",
       gridname      => "Jio-clusters",
  }
}
