node /^mssch\d{2}p1\.mum1\.r4g\.com$/ {
# Reconciliation Version (Final) - working

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "Auth-Subscriptions-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.R4G",
        servertype  => "mssch",
        hosttype    => "vm",
        stage       => runtime,
    }
     class { "java":
            jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
            jdk_ver => "present",
    }

    class { "tomcat":
        package            => "mobi-tomcat-config-8080",
        version            => "6.0.35-209592",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner       => "rtv",
        config_group       => "rtv",
        jars => [ "c3p0-0.9.2-pre2.jar", "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        initscript         => "tomcat8080",
        require            => Class["java"],
        jrehome            => "",
    }


    class { "nfs":
        client_ensure  => true,
        client_enable  => true,
        client_mounts => { 'ril_sapci' => 'sapci' },
        client_rsize   => "131072",
        client_wsize   => "524288",
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }


     class { "mobi_aaa_ril_sap_ci":
        sap_ci_version                      => "5.4.0-273627",
        offer_manager_endpoint_url          => "http://offermanagervip:8080/mobi-aaa-ril-offer-manager",
        purchase_database_url               => "jdbc:mysql://mydtbvip:3306/purchase_manager",
        rights_database_url                 => "jdbc:mysql://mydtbvip:3306/rights_manager",
        batch_database_url                  => "jdbc:mysql://mydtbvip:3306/sap_ci",
        database_username                   => "sap_ci_user",
        database_password                   => "mn_rkl48P_GJcaf",
        jersey_restclient_connectionTimeout => "30",
        jersey_restclient_readTimeout       => "30",
        jersey_restclient_maxConnections    => "300",
        jersey_restclient_enableClientLog   => "false",
        sapci_generate_payment_record_start_cron => "0 30 0,6,12,18 * * ?",
        sapci_generate_revenue_record_start_cron => "0 0 15,21 * * ?",
        pdr_paymentChannel_mobiApp          => "MOBI001",
        pdr_paymentChannel_selfcare         => "MOBI002",
        pdr_paymentMethod                   => "M3",
        pdr_merchantId                      => "1011012",
        pdr_terminalId                      => "video_application",
        record_sourceId                     => "mobi-sap-ci-adapter",
        max_retries                         => "2",
        batch_execution_count_perday        => "4",
        shared_filesystem_path              => "/var/Jukebox/sapci/",
        pdr_base_dir_name                   => "PDR",
        rdr_base_dir_name                   => "RDR",
        default_past_record_creation_window => "7",
        require => Class["tomcat"]
    }


}
