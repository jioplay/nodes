node /^msamq\d{2}p1\.mum1\.rjil\.com$/ {
# Reconciliation Version (Final) - working
    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "others-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "msamq",
        hosttype    => "vm",
        stage       => runtime,
    }

     class { "java" :
                jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
                jdk_ver => "present",
       }


    class {"activemq":
      package => "mobi-activemq-5.9.0-272600",
      config_path => "/opt/activemq/conf/activemq.xml",
      config => "activemq.udm",
    }


}
