node /^msxmp\d{2}p1\.mum1\.r4g\.com$/ {

    class { "base" :
        puppetagent_environment => "qa_ril_5_4_0",
        host_type               => "ril",
        manage_ganglia => true,
        gmond_cluster_name => "others-cluster",
    }

    class { "os::motd" :
        stack      => "ril-prod",
        owner      => "pinda ndaki",
        owneremail => "pndaki@mobitv.com",
        servertype => " in-app xmpp push server,",
        hosttype   => "VM",
        stage      => runtime,
    }


    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => present,
    }

    class { 'mobi_push_dispatcher':
        mobi_push_dispatcher_package => "mobi-push-dispatcher-5.5.0-271745",
        dispatcher_host_name         => $::fqdn,
        xmpp_domain_name             => 'xmpp.mobitv.com',
        require                      => Class["mobi_openfire"],
    }

    class { 'mobi_openfire':
        mobi_openfire_package        => 'mobi-openfire-3.8.0-271807',
        mobi_openfire_package_ensure => 'present',
        xmpp_domain_name             => 'xmpp.mobitv.com',
        admin_email                  => 'rajeev_warrier@persistent.co.in',
        memcached_servers => 'msliv03p1:11211,msliv04p1:11211',

    }

    class { 'mobi_openfire_external_authentication':
        package        => 'mobi-openfire-external-authentication-5.4.0-265834',
        package_ensure => 'present',
        require        => Class["mobi_openfire"],
    }

    class {'mobi_openfire_external_presence_plugin':
        mobi_openfire_external_presence_plugin_package        => 'mobi-openfire-external-presence-plugin-3.8.0-265834',
        mobi_openfire_external_presence_plugin_package_ensure => present,
        require                                               => Class["mobi_openfire"],
    }

      class {'mobi_openfire_websocket_plugin':
          mobi_openfire_websocket_plugin_package => 'mobi-openfire-websocket-plugin-3.8.0-265834',
          mobi_openfire_websocket_plugin_package_ensure => 'present',
          require => Class["mobi_openfire"],
      }

}
