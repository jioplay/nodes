node /^stfmv\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "vod-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "stfmv",
        hosttype    => "vm",
        stage       => runtime,
    }

    class { "apache": }

    class { "nfs":
        client_ensure  => true,
        client_enable  => true,
        client_mounts  => { 'ril_prod' => '/var/Jukebox' },
        client_rsize   => "131072",
        client_wsize   => "524288",
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }

    class { "fragserver":
        version  => "4.7.5-1422",
        require  => [Class["apache"], Class["nfs"]],

    }

    file {"/etc/httpd/conf.d/variants-ril.ini":
        owner   => "root",
        group   => "root",
        mode    => "0644",
        source  => "puppet:///modules/fragserver/ril/variants.ril.ini",
        require => [ Class["apache"], Class["fragserver"],]
    }

    file { "/etc/httpd/conf.d/variants-recordings.ini":
        owner   => "root",
        group   => "root",
        mode    => "0644",
        source  => "puppet:///modules/fragserver/ril/variants-recordings.prod.ini",
        require => [ Class["apache"], Class["fragserver"],]
    }

    $output_base_vod = "/var/Jukebox/vod/FMP4"

        fragserver::locations { "frag_vod.conf":
        content_root      => $output_base_vod,
        default_max_age   => "600",
        filter_bndw_box   => "On",
        fragment_duration => "6",
        live_buffer       => "3",
        location          => "/vod",
        set_env           => {'fmp4' => '1'},
        super_variants    => $super_variants_rec,
        variants_definition_file => "/etc/httpd/conf.d/variants-ril.ini",
    }

    $super_variants_rec = [["H2_W240_H264_MP","H3_W240_H264_MP","H5_W270_H264_MP","H8_W360_H264_MP","H13_W360_H264_MP","H17_W480_H264_MP","H31_W720_H264_MP","H2_S240_H264_MP","H3_S240_H264_MP","H5_S360_H264_MP","H8_S480_H264_MP","H13_S480_H264_MP","H17_S480_H264_MP","H24_S480_H264_MP" ]]

    $output_base_rec = "/var/Jukebox/recordings"

    fragserver::locations { "frag_recordings.conf":
        content_root      => $output_base_rec,
        default_max_age   => "600",
        filter_bndw_box   => "On",
        fragment_duration => "6",
        live_buffer       => "3",
        location          => "/recordings",
        set_env           => {'fmp4' => '1'},
        super_variants    => $super_variants_rec,
        variants_definition_file => "/etc/httpd/conf.d/variants-recordings.ini",
    }

    $packages =  ["mobi-logrotate-ods-config-0.2.0-1239", "mobi-urlauth-module-1.0.6-145094", "mobi-user-rtv-1.2.3-79211", "mobi-mps-1.6.0-264","mobi-mps-conf-mobi-1.6.0-265","mobi-ril-thumbnail-seek-rewrite-rules-5.3.10-201507061144.fa7b441",]

    package { $packages :
        ensure => present,
    }

}
