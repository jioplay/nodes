node /^juarn\d{2}p1\.mum1\.rjil\.com$/ {

    class { "base" :
        host_type               => "ril",
        manage_ganglia          => true,
        manage_icinga           => true,
        manage_puppet_config    => true,
        gmond_cluster_name      => "cms-cluster",
        puppetagent_environment => "qa_ril_5_4_0",
        use_pulp => false,
    }

    class { "os::motd" :
        owner       => "MobiTV Operations",
        owneremail  => "ril-support@mobitv.com",
        stack       => "MUM1.RJIL",
        servertype  => "Jukebox Arlanda CMS",
        hosttype    => "vm",
        stage       => runtime,
    }
     class { "os::hosts":
        stage => setup,
        remote_hosts => [ "10.139.22.216  v-ls-rcp12",],
    }


#    class { "mobi_cms_us::filesystem2":
#      external_filesystem => true,
#      require => Class["nfs"],
#    }

    class { "apache":
        package => "httpd.x86_64",
    }

    class{ "mediainfo":
        mediainfo_version    => "0.7.58-277.1",
        libmediainfo_version => "0.7.58-267.1",
        libzen_version       => "0.4.26-152.1",
    }

    class { "nfs":
        client_ensure  => true,
        client_enable  => true,
        client_mounts  => { 'ril_prod' => '/var/Jukebox' },
        client_rsize   => "131072",
        client_wsize   => "524288",
        nfsutil_ensure => latest,
        autofs_ensure  => latest,
        portmap_ensure => latest,
    }

    class { "java" :
        jdk_pkg => "jdk-1.6.0_33-fcs.x86_64",
        jdk_ver => "present",
    }

    class { "tomcat":
        package => "mobi-tomcat-config-8080-6.0.35-211717.noarch",
        version => "present",
        override_catalina  => false,
        server_config_loc  => "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/server.xml",
        server_config_src  => "server.xml.tomcat8080",
        config_owner => "rtv",
        config_group => "rtv",
        jars => [ "c3p0-0.9.2-pre2.jar", "mchange-commons-java-0.2.1.jar", "mysql-connector-java-5.1.19-bin.jar" ],
        initscript => "tomcat8080",
        require => Class["java"],
        jrehome => "",
    }

   class { "mobi_policy_manager::juarn":
        policy_manager_version => "5.4.9-267117",
        min_pool_size          => "100",
        max_pool_size          => "555",
        policy_username        => "pm_user",
        policy_password        => "sld9_wlori5TER_k8d",
        policy_db_jdbc_url     => "jdbc:mysql://mydtbvip:3306/POLICY_MGR?autoReconnect=true",
        ril                    => true,
        notify                 => Class["tomcat::service"],
        require                => Class["tomcat"],
    }

    class { "fragwriter":
        package   => "mobi-fragwriter-4.8.1-1488.x86_64",
    }

    class { "mobi_arlanda_dam":
        version             => "5.4.0-266994",
        include_fm_resource => false,
        dam_db_username     => "dam_user",
        dam_db_password     => "n5_2_djTsa,vcdj",
        ril                 => true,
        notify  => Class["tomcat::service"],
        require => Class["tomcat"],
    }
    class { "mobi_arlanda_wfm":
        version                        => "5.4.0-277968",
        ril                            => true,
        provider_files                 => ["2","3","4","5","6","7","8",],
        notify                         => Class["tomcat::service"],
        require                        => Class["tomcat"],
        db_username                    => "wfm_user",
        db_password                    => "ms_ksoDSdoe3maP", 
        #vantage_workflow_name          => "Star_TvShow",
        vantage_to_platform_xsl        => "/var/Jukebox/uploads/search_transform/vantagetoplatform.xsl",
        adspot_basedir                 => "/var/Jukebox/vantage/Output/adspot/",
        provider_adspot_converter_path => "/var/Jukebox/uploads/search_transform/adspot/",
        provider_metadata_path         => "/var/Jukebox/uploads/metadata/",
    }

    class { "mobi_arlanda_mp":
        version     => "5.4.0-270101",
        notify      => Class["tomcat::service"],
        ril         => true,
        db_username => "mp_user",
        db_password => "ovmw_pF_7bFcnie",
        require     => Class["tomcat"],
        fragment_queue_name => "FRAGMENTATION.QUEUE",
    }

    package { "mobi-mfe":
        ensure => "4.5.0-1149",
    }

    package { "mobi-arlanda-transcoder" :
        ensure => "5.0.1-218681",
    }

    class { "mobi_arlanda_fragfileprocessor":
        version              => '5.4.1-276733',
        drm1_enabled         => false,
        drm2_enabled         => true,
        fmp4_input_encodings => '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65',
        encodings_file       => 'encodings.properties.ril',
        offer_management_url => 'http://offermanagervip:8080/mobi-aaa-ril-offer-manager/guide/v5/asset/vod/{MID}.json?includeClipChannels=true',
        license_file_encoding_id => "501",
        folder_manager_rest_url  => "http://juarnvip:8080/dam/",
        segmenter_timeout        => "1500",
        offer_management_timeout => "1000",
        notify               => Class['tomcat::service'],
        require              => [ Class["tomcat"], Package["mobi-mfe"], ],
    }

   
    $encoding_array = ["H2_S288_H264_BP:encoding-401","H3_S288_H264_BP:encoding-402","H5_S432_H264_BP:encoding-403","H8_S576_H264_BP:encoding-404","H13_S576_H264_BP:encoding-405","H17_S576_H264_BP:encoding-406","H2_S288_H264_MP:encoding-407","H3_S288_H264_MP:encoding-408","H5_S432_H264_MP:encoding-409","H8_S576_H264_MP:encoding-410","H13_S576_H264_MP:encoding-411","H17_S576_H264_MP:encoding-412","H24_S576_H264_MP:encoding-413","H24_S576_H264_HP:encoding-414","H2_S240_H264_BP:encoding-415","H3_S240_H264_BP:encoding-416","H5_S360_H264_BP:encoding-417","H8_S480_H264_BP:encoding-418","H13_S480_H264_BP:encoding-419","H17_S480_H264_BP:encoding-420","H2_S240_H264_MP:encoding-421","H3_S240_H264_MP:encoding-422","H5_S360_H264_MP:encoding-423","H8_S480_H264_MP:encoding-424","H13_S480_H264_MP:encoding-425","H17_S480_H264_MP:encoding-426","H24_S480_H264_MP:encoding-427","H24_S480_H264_HP:encoding-428","H2_W240_H264_BP:encoding-429","H3_W240_H264_BP:encoding-430","H5_W270_H264_BP:encoding-431","H8_W360_H264_BP:encoding-432","H13_W360_H264_BP:encoding-433","H17_W480_H264_BP:encoding-434","H2_W240_H264_MP:encoding-435","H3_W240_H264_MP:encoding-436","H5_W270_H264_MP:encoding-437","H8_W360_H264_MP:encoding-438","H13_W360_H264_MP:encoding-439","H17_W480_H264_MP:encoding-440","H24_W720_H264_MP:encoding-441","H24_W720_H264_HP:encoding-442","H31_W720_H264_MP:encoding-443","H31_W720_H264_HP:encoding-444","H41_W720_H264_MP:encoding-445","H41_W720_H264_HP:encoding-446","H41_W1080_H264_MP:encoding-447","H41_W1080_H264_HP:encoding-448","H24_W480_H264_BP:encoding-449","H31_W480_H264_BP:encoding-450","H24_W480_H264_MP:encoding-451","H31_W480_H264_MP:encoding-452","H17_W360_H264_BP:encoding-453","H24_W360_H264_BP:encoding-454","H17_W360_H264_MP:encoding-455","H24_W360_H264_MP:encoding-456","H8_W270_H264_BP:encoding-458","H12_W270_H264_BP:encoding-459","H8_W270_H264_MP:encoding-461","H12_W270_H264_MP:encoding-462","H31_W1080_H264_HP:encoding-463","H91_W1080_H264_HP:encoding-464","H91_W720_H264_HP:encoding-465"]

#   nfs::generate_encodings{ $encoding_array: encoding => "FMP4", require => [Class["nfs"]], }

}
