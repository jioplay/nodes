# WARNING: modification of the base class or default node definition WILL result
# in changes to QA/Eng!

class base (
    $use_pulp = false,
    $yum_server = "inyum02p1.mum1.r4g.com",
    $host_type = "ril",
    $manage_puppet = true,
    $manage_puppet_config = true,
    $puppetagent_environment =  "qa_ril_5_4_0",
    $puppetagent_version = "2.7.21-1.el${operatingsystemmajrelease}",
    $puppetagent_runinterval = "3600",
    $puppetagent_noop = false,

    $manage_icinga = false,

    $manage_ganglia = false,
    $gmond_cluster_name = "ril",

    $manage_mcollective = false,

    $sendmail_service_ensure = running,
    $sendmail_service_enable = true,

    $snmpd_service_ensure = stopped,
    $snmpd_service_enable = false,

    $manage_rsyslog = true,
    $manage_syslog_ng = false,
    $manage_syslog_ng_config = true,
    $use_default_limits = true,
) {

    class { "stdlib": }

    class { "ntpd": }

    # default accounts
    include accounts
    realize(Accounts::Group["arlanda"])
    realize(Accounts::Group["rtv"])
    realize(Accounts::User["rtv"])
    realize(Accounts::User["apache"])

    class { "os":
        sendmail_service_ensure => $sendmail_service_ensure,
        sendmail_service_enable => $sendmail_service_enable,
        snmpd_service_ensure    => $snmpd_service_ensure,
        snmpd_service_enable    => $snmpd_service_enable,
	resolv_nameservers	=> [ "10.139.24.112", "10.139.24.113" ],
	resolv_search		=> "mum1.r4g.com",
    }

    class { "os::git_branch_commit": stage => setup}

    class { "yum::ril" :
        server => $yum_server,
        stage  => setup,
    }

    class { "sudo":
        users   => ["vagrant"],
        groups  => ["mobiops"],
        stage   => setup,
    }

    if ($manage_rsyslog) {
        class { "rsyslog": }
    } elsif ! ($manage_syslog_ng) {
        class { "rsyslog": }
    } else {
        class { "syslog_ng":
            manage_config => $manage_syslog_ng_config,
            config        => "syslog-ng.conf.ril",
        }
    }

#    if $::virtual == "vmware" {
#        include vmware_tools
#    }

    if $manage_puppet {
        class { "puppetagent":
            package_ensure  => $puppetagent_version,
            server_name     => "puppet3.mum1.r4g.com",
            manage_config   => $manage_puppet_config,
            environment     => $puppetagent_environment,
            runinterval     => $puppetagent_runinterval,
            icinga          => $manage_icinga,
            icinga_instance => "icinga.mum1.r4g.com",
            noop            => $puppetagent_noop,
            rsyslog         => true,
        }
    }
    if ($use_default_limits) {
        class { "os::limitsd": }

    }

    include concat::setup
    include stdlib
    # soaking mcollective in mum1.r4g.com domain
    if ($manage_mcollective) and ($::architecture == "x86_64") and ($::operatingsystemrelease >= 5) {
        class { "mcollective":
            stomp_server => "mcollective.mum1.r4g.com",
        }
    }

    if $manage_icinga {
        # add this node to icinga
        class { "icinga::register":
            icinga_instance => "icinga.mum1.r4g.com",
        }

        # install nrpe and perform client-side checks
        class { "icinga::nrpe":
            icinga_instance => "icinga.mum1.r4g.com",
            add_nagios_exchange_checks => true,
        }
    }

    if $manage_ganglia {
        if ! $gmond_cluster_name {
            fail("\$gmond_cluster_name is a required parameter to ${name}.")
        }

        # install gmond and setup metric collection
        class { "ganglia::gmond":
            cluster_name         => $gmond_cluster_name,
            gmond_package_ensure => "latest",
        }
    }
}
