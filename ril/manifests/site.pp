# base node/class manifests
import "base.d/*.pp"

# node manifests organized by D.C.
import "sites.d/mum1.r4g.com.d/*.pp"

# Makes troubleshooting incorrect node names easier, also safer.
node default {
    fail("No node definition available for this host!")
}
